package com.example;

import com.example.model.StudentModel;
import com.example.service.StudentService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Get-Up on 5/10/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests {
    @Autowired
    StudentService studentService;

    @Test
    public void testSelectAllStudent() {
        List<StudentModel> students = studentService.selectAllStudents();
        Assert.assertNotNull("Gagal - students menghasilkan null", students);
        Assert.assertEquals("Gagal - size students tidak sesuai", 2, students.size());
    }

    @Test
    public void testSelectStudent() {
        StudentModel student = studentService.selectStudent("123");
        Assert.assertNotNull("Gagal - student dengan NPM 123 tidak ditemukan", student);
        Assert.assertEquals("Gagal - nama student pada databases adalah Kamu", "Kamu", student.getName());
        Assert.assertEquals("Gagal - GPA student pada database adalah 3.45", 3.45, student.getGpa(), 0.0);

    }

    @Test
    public void testCreateStudent() {
        StudentModel student = new StudentModel("126", "Budi", 3.44, null);
        // Cek apakah student sudah ada
        Assert.assertNull("Mahasiswa sudah ada", studentService.selectStudent(student.getNpm()));
        // Masukkan ke service
        studentService.addStudent(student);
        // Cek apakah student berhasil dimasukkan
        Assert.assertNotNull("Mahasiswa gagal dimasukkan", studentService.selectStudent(student.getNpm()));
    }

    @Test
    public void testUpdateStudent() {
        StudentModel student = studentService.selectStudent("123");
        Assert.assertNotNull("Gagal - student dengan NPM 123 tidak ditemukan", student);

        student.setName("nama saya ini");
        studentService.updateStudent(student);
        Assert.assertEquals("Gagal - nama student pada databases tidak berubah menjadi 'nama saya ini'", "nama saya ini", student.getName());
    }

    @Test
    public void testDeleteStudent() {
        StudentModel student = studentService.selectStudent("123");
        Assert.assertNotNull("Gagal - student dengan NPM 123 tidak ditemukan", student);

        studentService.deleteStudent("123");
        student = studentService.selectStudent("123");
        Assert.assertNull("Gagal - Student dengan NPM 123 tidak dapat dihapus",student);
    }
}
