package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.CourseModel;
import com.example.service.CourseService;

import java.util.List;

@Controller
public class CourseController {
	@Autowired
	CourseService courseDAO;
	
	@RequestMapping("/course/view/{id}")
	public String viewPath(Model model, @PathVariable(value = "id") String idCourse) {
		CourseModel course = courseDAO.selectCourse(idCourse);

		if (course != null) {
			model.addAttribute("course", course);
			return "view-course";
		} else {
			model.addAttribute("idCourse", idCourse);
			return "not-found-course";
		}
	}

	@RequestMapping("/course/viewall")
	public String view(Model model) {
		List<CourseModel> courses = courseDAO.selectAllCourses();
		model.addAttribute("courses", courses);

		return "viewall-courses";
	}
}
