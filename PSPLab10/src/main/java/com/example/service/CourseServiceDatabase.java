package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.CourseMapper;
import com.example.model.CourseModel;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Service
public class CourseServiceDatabase implements CourseService {
    @Autowired
    private CourseMapper courseMapper;

    @Override
    public List<CourseModel> selectAllCourses() {
        List<CourseModel> results = courseMapper.selectAllCourse();
        log.info("select all course");
        return results;
    }

    @Override
    public CourseModel selectCourse(String idCourse) {
        CourseModel result = courseMapper.selectCourse(idCourse);
        log.info("select course with id_course {}", idCourse);
        log.info("Jumlah mhs yang mengambil {} ada sebanyak {}", idCourse, result.getStudents().size());
        return result;
    }

}
