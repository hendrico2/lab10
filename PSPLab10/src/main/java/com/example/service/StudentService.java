package com.example.service;

import java.util.List;

import com.example.model.StudentModel;

public interface StudentService {
	StudentModel selectStudent(String npm);

	List<StudentModel> selectAllStudents();

	void addStudent(StudentModel student);

	int deleteStudent(String npm);
	
	void updateStudent(StudentModel student);
}
